       /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

       //    var gObjectCourses = {
       //        description: "This DB includes all courses in system",
       //        courses: [{
       //            id: 1,
       //            courseCode: "FE_WEB_ANGULAR_101",
       //            courseName: "How to easily create a website with Angular",
       //            price: 750,
       //            discountPrice: 600,
       //            duration: "3h 56m",
       //            level: "Beginner",
       //            coverImage: "images/courses/course-angular.jpg",
       //            teacherName: "Morris Mccoy",
       //            teacherPhoto: "images/teacher/morris_mccoy.jpg",
       //            isPopular: false,
       //            isTrending: true
       //        }, {
       //            id: 2,
       //            courseCode: "BE_WEB_PYTHON_301",
       //            courseName: "The Python Course: build web application",
       //            price: 1050,
       //            discountPrice: 900,
       //            duration: "4h 30m",
       //            level: "Advanced",
       //            coverImage: "images/courses/course-python.jpg",
       //            teacherName: "Claire Robertson",
       //            teacherPhoto: "images/teacher/claire_robertson.jpg",
       //            isPopular: false,
       //            isTrending: true
       //        }, {
       //            id: 5,
       //            courseCode: "FE_WEB_GRAPHQL_104",
       //            courseName: "GraphQL: introduction to graphQL for beginners",
       //            price: 850,
       //            discountPrice: 650,
       //            duration: "2h 15m",
       //            level: "Intermediate",
       //            coverImage: "images/courses/course-graphql.jpg",
       //            teacherName: "Ted Hawkins",
       //            teacherPhoto: "images/teacher/ted_hawkins.jpg",
       //            isPopular: true,
       //            isTrending: false
       //        }, {
       //            id: 6,
       //            courseCode: "FE_WEB_JS_210",
       //            courseName: "Getting Started with JavaScript",
       //            price: 550,
       //            discountPrice: 300,
       //            duration: "3h 34m",
       //            level: "Beginner",
       //            coverImage: "images/courses/course-javascript.jpg",
       //            teacherName: "Ted Hawkins",
       //            teacherPhoto: "images/teacher/ted_hawkins.jpg",
       //            isPopular: true,
       //            isTrending: true
       //        }, {
       //            id: 8,
       //            courseCode: "FE_WEB_CSS_111",
       //            courseName: "CSS: ultimate CSS course from beginner to advanced",
       //            price: 750,
       //            discountPrice: 600,
       //            duration: "3h 56m",
       //            level: "Beginner",
       //            coverImage: "images/courses/course-css.jpg",
       //            teacherName: "Juanita Bell",
       //            teacherPhoto: "images/teacher/juanita_bell.jpg",
       //            isPopular: true,
       //            isTrending: true
       //        }, {
       //            id: 14,
       //            courseCode: "FE_WEB_WORDPRESS_111",
       //            courseName: "Complete Wordpress themes & plugins",
       //            price: 1050,
       //            discountPrice: 900,
       //            duration: "4h 30m",
       //            level: "Advanced",
       //            coverImage: "images/courses/course-wordpress.jpg",
       //            teacherName: "Clevaio Simon",
       //            teacherPhoto: "images/teacher/clevaio_simon.jpg",
       //            isPopular: true,
       //            isTrending: false
       //        }]
       //    }

       const gCOURSE_COL = ["courseCode", "courseName", "price", "discountPrice", "duration", "level", "coverImage", "teacherName", "teacherPhoto", "isPopular", "isTrending", "action"];

       var gObjectCourses = [];

       const gCOURSE_CODE_COL = 0;
       const gCOURSE_NAME_COL = 1;
       const gPRICE_COL = 2;
       const gDISCOUNT_PRICE_COL = 3;
       const gDURATION_COL = 4;
       const gLEVEL_COL = 5;
       const gCOVER_IMAGE_COL = 6;
       const gTEACHER_NAME_COL = 7;
       const gTEACHER_PHOTO_COL = 8;
       const gIS_POPULAR_COL = 9;
       const gIS_TRENDING_COL = 10;
       const gACTION_COL = 11;

       //Biến lưu ID Courses
       var gIdCourseEdit = [];
       var gIdCourseDelete = [];
       var gCourseCode = [];


       /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
       $(document).ready(function() {
           callApiGetServer();
           onPageLoading();
           onLoadCoursesToTable(gObjectCourses.data);

           // 2 - C: Gán sự kiện Create - Thêm mới courses
           $("#btn-add-courses").on("click", function() {
               onBtnAddNewCoursesClick();
           });
           // gán sự kiện cho nút Create Course (trên modal)
           $("#btn-create-courses").on("click", function() {
               onBtnConfirmCreateCourse();
           });



           // 3 - U: gán sự kiện Update - Sửa 1 Course
           $("#courses-table").on("click", ".btn-edit-course", function() {
               onBtnEditCourseClick(this);
           });

           // gán sự kiện cho nút Update Course (trên modal)
           $("#btn-upDate-course").on("click", function() {
               onBtnUpdateCourseClick();
           });



           // 4 - D: gán sự kiện Delete - Xóa 1 Course
           $("#courses-table").on("click", ".btn-delete-course", function() {
               onBtnDeleteCourseClick(this);
           });

           $("#btn-confirm-delete-course").on("click", function() {
               onBtnConfirmDeleteCourseClick();
           });
       });



       /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

       function onPageLoading() {
           "use strict";

           $('#courses-table').DataTable({

               //    data: gObjectCourses.courses,
               columns: [{
                   data: gCOURSE_COL[gCOURSE_CODE_COL]
               }, {
                   data: gCOURSE_COL[gCOURSE_NAME_COL]
               }, {
                   data: gCOURSE_COL[gPRICE_COL]
               }, {
                   data: gCOURSE_COL[gDISCOUNT_PRICE_COL]
               }, {
                   data: gCOURSE_COL[gDURATION_COL]
               }, {
                   data: gCOURSE_COL[gLEVEL_COL]
               }, {
                   data: gCOURSE_COL[gCOVER_IMAGE_COL]
               }, {
                   data: gCOURSE_COL[gTEACHER_NAME_COL]
               }, {
                   data: gCOURSE_COL[gTEACHER_PHOTO_COL]
               }, {
                   data: gCOURSE_COL[gIS_POPULAR_COL]
               }, {
                   data: gCOURSE_COL[gIS_TRENDING_COL]
               }, {
                   data: gCOURSE_COL[gACTION_COL]
               }],

               columnDefs: [{
                       targets: gACTION_COL,
                       defaultContent: `
                        <i class="fas fa-edit text-success pointer ml-3 btn btn-edit-course" data-toggle="tooltip" title="Sửa đơn hàng"></i>
                        <i class="fas fa-trash-alt text-danger pointer ml-3 btn btn-delete-course" data-toggle="tooltip" title="Xóa đơn hàng"></i>
                `
                   },
                   {
                       targets: gCOVER_IMAGE_COL,
                       className: "text-center",
                       render: function(data, type, row, neta) {
                           return '<img class="img-thumbnail" src="' + data + '" height="100" width="130"/>';
                       }
                   },
                   {
                       targets: gTEACHER_PHOTO_COL,
                       className: "text-center",
                       render: function(data, type, row, neta) {
                           return '<img class="img-thumbnail" src="' + data + '" height="100" width="130"/>';
                       }
                   },
               ]
           });

           onLoadCoursesToTable(gObjectCourses.data);
       }
       /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

       //Call API data
       function callApiGetServer() {
           $.ajax({
               url: "/courses",
               type: "GET",
               dataType: "json",
               async: false,
               success: function(responseJson) {
                   console.log(responseJson);
                   gObjectCourses = responseJson
                   console.log(gObjectCourses.data)
               },
               error: function(ajaxContent) {
                   console.log(ajaxContent);
               }
           })
       }

       //Hàm Load Table dùng chung cho các hàm
       function onLoadCoursesToTable(paramData) {
           var vTable = $('#courses-table').DataTable();
           vTable.clear();
           vTable.rows.add(paramData);
           vTable.draw();
       }




       // CÁC HÀM CHO ADD NEW VÀ CREATE
       // Hàm sự kiện nút add new
       function onBtnAddNewCoursesClick() {
           $('#insert-create-courses-modal').modal('show');
       }

       // Hàm sự kiện nút creat - thêm dữ liệu mới
       function onBtnConfirmCreateCourse() {
           // tạo biến lưu thông tin từ input
           var gConfirmButton = {
               courseCode: "",
               courseName: "",
               price: 0,
               discountPrice: 0,
               duration: "",
               level: "",
               coverImage: "",
               teacherName: "",
               teacherPhoto: "",
               isPopular: "",
               isTrending: ""
           };
           // đọc thông tin từ input
           readDataFromCreate(gConfirmButton);
           // hàm validate
           var vCheckData = validateDataConfirm(gConfirmButton);
           // nếu validate = true thì chạy tiếp
           if (vCheckData == true) {
               // thêm data vào bảng
               callApiPostData(gConfirmButton)
               console.log(gConfirmButton)
                   // load trang
               alert("Tạo thông tin mới thành công!");
               onLoadCoursesToTable(gObjectCourses.data);
               // xóa trắng bảng
               resertCreateCoursesForm();
               // ẩn modal
               $("#insert-create-courses-modal").modal('hide');
           }
       }

       // hàm thu thập dữ liệu để insert courses
       function readDataFromCreate(paramCreateObj) {
           paramCreateObj.courseCode = $("#inp-create-courseCode").val().trim();
           paramCreateObj.courseName = $("#inp-create-courseName").val().trim();
           paramCreateObj.price = Number($("#inp-create-price").val().trim());
           paramCreateObj.discountPrice = Number($("#inp-create-discountPrice").val().trim());
           paramCreateObj.duration = $("#inp-create-duration").val().trim();
           paramCreateObj.level = $("#inp-create-level").val().trim();
           paramCreateObj.coverImage = $("#inp-create-coverImage").val().trim();
           paramCreateObj.teacherName = $("#inp-create-teacherName").val().trim();
           paramCreateObj.teacherPhoto = $("#inp-create-teacherPhoto").val().trim();
           paramCreateObj.isPopular = $("#select-create-popular").val();
           paramCreateObj.isTrending = $("#select-create-trending").val();
       }


       function validateDataConfirm(paramCourseObj) {

           if (paramCourseObj.courseCode == "") {
               console.log("Dữ liệu ô Course Code không được để trống");
               alert("Dữ liệu ô Course Code không được để trống");
               return false;
           }

           if (paramCourseObj.courseName == "") {
               console.log("Dữ liệu ô Course Name không được để trống");
               alert("Dữ liệu ô Course Name không được để trống");
               return false;
           }

           if (paramCourseObj.price == "" || paramCourseObj.price < 1) {
               console.log("Dữ liệu ô Price không đúng");
               alert("Dữ liệu ô Price không đúng");
               return false;
           }

           if (isNaN(paramCourseObj.price)) {
               console.log("Price phải nhập số");
               alert("Price phải nhập số");
               return false;
           }



           if (paramCourseObj.discountPrice == "" || paramCourseObj.discountPrice < 0) {
               console.log("Dữ liệu ô Discount Price không đúng");
               alert("Dữ liệu ô Discount Price không đúng");
               return false;
           }

           if (isNaN(paramCourseObj.discountPrice)) {
               console.log("Discount Price phải nhập số");
               alert("Discount Price phải nhập số");
               return false;
           }



           if (paramCourseObj.duration == "") {
               console.log("Dữ liệu ô Duration không được để trống");
               alert("Dữ liệu ô Duration không được để trống");
               return false;
           }

           if (paramCourseObj.level == "") {
               console.log("Dữ liệu ô level không được để trống");
               alert("Dữ liệu ô level không được để trống");
               return false;
           }

           if (paramCourseObj.coverImage == "") {
               console.log("Dữ liệu ô coverImage không được để trống");
               alert("Dữ liệu ô coverImage không được để trống");
               return false;
           }

           if (paramCourseObj.teacherName == "") {
               console.log("Dữ liệu ô Teacher Name không được để trống");
               alert("Dữ liệu ô Teacher Name không được để trống");
               return false;
           }

           if (paramCourseObj.teacherPhoto == "") {
               console.log("Dữ liệu ô Teacher Photo không được để trống");
               alert("Dữ liệu ô Teacher Photo không được để trống");
               return false;
           }

           if (paramCourseObj.isPopular == "NOT_SELECT_POPULAR") {
               console.log("Hãy chọn trạng thái ô Popular");
               alert("Hãy chọn trạng thái ô Popular");
               return false;
           }

           if (paramCourseObj.isTrending == "NOT_SELECT_TRENDING") {
               console.log("Hãy chọn trạng thái ô Trending");
               alert("Hãy chọn trạng thái ô Trending");
               return false;
           }

           return true;
       }

       // hàm thực hiện insert Course vào mảng
       function callApiPostData(paramConfirmButton) {
           $.ajax({
               url: "/courses",
               type: "POST",
               data: JSON.stringify(paramConfirmButton),
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: function(res) {
                   console.log(res)
                   location.reload();
               },
               error: function(res) {
                   console.log(res.status)
               }
           });
       }

       // hàm xóa trắng form modal create Course
       function resertCreateCoursesForm() {
           $("#inp-create-courseCode").val("");
           $("#inp-create-courseName").val("");
           $("#inp-create-price").val("");
           $("#inp-create-discountPrice").val("");
           $("#inp-create-duration").val("");
           $("#inp-create-level").val("");
           $("#inp-create-coverImage").val("");
           $("#inp-create-teacherName").val("");
           $("#inp-create-teacherPhoto").val("");
           $("#select-create-popular").val("");
           $("#select-create-trending").val("");
       }



       // CÁC HÀM CHO EDIT VÀ UPDATE

       // hàm xử lý sự kiện edit courses modal click
       //EDIT
       function onBtnEditCourseClick(paramEdit) {

           // load dữ liệu vào các trường dữ liệu trong modal
           showCoursesDataToModal(paramEdit);

           // hiển thị modal lên
           $("#insert-edit-courses-modal").modal("show");
       }

       // hàm show courses obj lên modal
       function showCoursesDataToModal(paramUpdate) {
           var vCurrentTr = $(paramUpdate).closest("tr");
           var vTable = $("#courses-table").DataTable();
           var vDataRow = vTable.row(vCurrentTr).data();
           vCourseCode = vDataRow.courseCode
           console.log(vCourseCode)
           var vObjectRequest = gObjectCourses.data
           vObjectRequest.filter(function(index, objectData) {
               console.log(index)
               if (index.courseCode === vCourseCode) {
                   vIdCourseEdit = index._id
                   console.log(vIdCourseEdit)
               }
           })
           $("#inp-edit-courseCode").val(vDataRow.courseCode);
           $("#inp-edit-courseName").val(vDataRow.courseName);
           $("#inp-edit-price").val(vDataRow.price);
           $("#inp-edit-discountPrice").val(vDataRow.discountPrice);
           $("#inp-edit-duration").val(vDataRow.duration);
           $("#inp-edit-level").val(vDataRow.level);
           $("#inp-edit-coverImage").val(vDataRow.coverImage);
           $("#inp-edit-teacherName").val(vDataRow.teacherName);
           $("#inp-edit-teacherPhoto").val(vDataRow.teacherPhoto);
           $("#select-edit-popular").val(String(vDataRow.isPopular));
           $("#select-edit-trending").val(String(vDataRow.isTrending));
       }

       //UPDATE
       // hàm xử lý sự kiện update courses modal click
       function onBtnUpdateCourseClick() {

           // khai báo đối tượng chứa courses data
           var vCoursesObj = {
               courseCode: "",
               courseName: "",
               price: 1,
               discountPrice: 1,
               duration: "",
               level: "",
               coverImage: "",
               teacherName: "",
               teacherPhoto: "",
               isPopular: "",
               isTrending: ""
           }

           // B1: Thu thập dữ liệu
           getCoursesDataUpdate(vCoursesObj)

           // B2: Validate insert
           var vIsCourseValidate = validateCourseDataUpdate(vCoursesObj);
           if (vIsCourseValidate) {

               // B3: insert course
               updateDataCourse(vCoursesObj);

               callApiUpdateCourse(vCoursesObj);

               // B4: xử lý front-end
               alert("Sửa thông tin thành công!");
               onLoadCoursesToTable(gObjectCourses.data);
               resertUpdateCoursesForm();
               $('#insert-edit-courses-modal').modal('hide');
           }
       }

       function getCoursesDataUpdate(paramCourseObj) {

           paramCourseObj.id = vIdCourseEdit;
           paramCourseObj.courseCode = $("#inp-edit-courseCode").val();
           paramCourseObj.courseName = $("#inp-edit-courseName").val();
           paramCourseObj.price = parseInt($("#inp-edit-price").val());
           paramCourseObj.discountPrice = parseInt($("#inp-edit-discountPrice").val());
           paramCourseObj.duration = $("#inp-edit-duration").val().trim();
           paramCourseObj.level = $("#inp-edit-level").val().trim();
           paramCourseObj.coverImage = $("#inp-edit-coverImage").val().trim();
           paramCourseObj.teacherName = $("#inp-edit-teacherName").val().trim();
           paramCourseObj.teacherPhoto = $("#inp-edit-teacherPhoto").val().trim();
           paramCourseObj.isPopular = $("#select-edit-popular").val();
           paramCourseObj.isTrending = $("#select-edit-trending").val();
       }

       function validateCourseDataUpdate(paramCourseObj) {

           if (paramCourseObj.courseName === "") {
               console.log("Dữ liệu ô Course Name không được để trống");
               alert("Dữ liệu ô Course Name không được để trống");
           }

           if (paramCourseObj.price === "" || paramCourseObj.price < 1) {
               console.log("Dữ liệu ô Price không đúng");
               alert("Dữ liệu ô Price không đúng");
               return false;
           }

           if (isNaN(paramCourseObj.price)) {
               console.log("Price phải nhập số");
               alert("Price phải nhập số");
               return false;
           }

           if (paramCourseObj.discountPrice === "" || paramCourseObj.discountPrice < 0) {
               console.log("Dữ liệu ô Discount Price không đúng");
               alert("Dữ liệu ô Discount Price không đúng");
               return false;
           }

           if (isNaN(paramCourseObj.discountPrice)) {
               console.log("Discount Price phải nhập số");
               alert("Discount Price phải nhập số");
               return false;
           }

           if (paramCourseObj.duration === "") {
               console.log("Dữ liệu ô Duration không được để trống");
               alert("Dữ liệu ô Duration không được để trống");
               return false;
           }

           if (paramCourseObj.level === "") {
               console.log("Dữ liệu ô level không được để trống");
               alert("Dữ liệu ô level không được để trống");
               return false;
           }

           if (paramCourseObj.coverImage === "") {
               console.log("Dữ liệu ô coverImage không được để trống");
               alert("Dữ liệu ô coverImage không được để trống");
               return false;
           }

           if (paramCourseObj.teacherName === "") {
               console.log("Dữ liệu ô Teacher Name không được để trống");
               alert("Dữ liệu ô Teacher Name không được để trống");
               return false;
           }

           if (paramCourseObj.teacherPhoto === "") {
               console.log("Dữ liệu ô Teacher Photo không được để trống");
               alert("Dữ liệu ô Teacher Photo không được để trống");
               return false;
           }

           if (paramCourseObj.isPopular === "") {
               console.log("Dữ liệu ô Popular không được để trống");
               alert("Dữ liệu ô Teacher Popular không được để trống");
               return false;
           }

           if (paramCourseObj.isTrending === "") {
               console.log("Dữ liệu ô Trending không được để trống");
               alert("Dữ liệu ô Trending không được để trống");
               return false;
           }

           return true;
       }

       // hàm sửa thông tin khóa học đã sửa vào obj
       function updateDataCourse(paramConfirmButtonUpdate) {
           for (var bI = 0; bI < gObjectCourses.data.length; bI++) {
               if (gObjectCourses.data[bI].id === paramConfirmButtonUpdate.id) {
                   gObjectCourses.data[bI].courseCode = paramConfirmButtonUpdate.courseCode;
                   gObjectCourses.data[bI].courseName = paramConfirmButtonUpdate.courseName;
                   gObjectCourses.data[bI].price = paramConfirmButtonUpdate.price;
                   gObjectCourses.data[bI].discountPrice = paramConfirmButtonUpdate.discountPrice;
                   gObjectCourses.data[bI].duration = paramConfirmButtonUpdate.duration;
                   gObjectCourses.data[bI].level = paramConfirmButtonUpdate.level;
                   gObjectCourses.data[bI].coverImage = paramConfirmButtonUpdate.coverImage;
                   gObjectCourses.data[bI].teacherName = paramConfirmButtonUpdate.teacherName;
                   gObjectCourses.data[bI].teacherPhoto = paramConfirmButtonUpdate.teacherPhoto;
                   gObjectCourses.data[bI].isPopular = paramConfirmButtonUpdate.isPopular;
                   gObjectCourses.data[bI].isTrending = paramConfirmButtonUpdate.isTrending;
               }
           }
       }

       function callApiUpdateCourse(paramCourseObj) {
           $.ajax({
               url: "/courses/" + vIdCourseEdit,
               type: "PUT",
               dataType: "json",
               data: JSON.stringify(paramCourseObj),
               contentType: "application/json; charset=utf-8",
               success: function(data) {
                   console.log(data);
                   location.reload();
               },
           })
       }
       // hàm xóa trắng form modal update courses
       function resertUpdateCoursesForm() {
           $("#inp-edit-id").html("");
           $("#inp-edit-courseCode").html("");
           $("#inp-edit-courseName").val("");
           $("#inp-edit-price").val("");
           $("#inp-edit-discountPrice").val("");
           $("#inp-edit-duration").val("");
           $("#inp-edit-level").val("");
           $("#inp-edit-coverImage").val("");
           $("#inp-edit-teacherName").val("");
           $("#inp-edit-teacherPhoto").val("");
           $("#select-edit-popular").val("");
           $("#select-edit-trending").val("");
       }


       // CÁC HÀM CHO DELETE

       // Hàm xử lý sự kiện khi icon delete voucher trên bảng đc click
       function onBtnDeleteCourseClick(paramBtnDelete) {

           var vCurrentTr = $(paramBtnDelete).closest("tr");
           var vTable = $("#courses-table").DataTable();
           var vDataRow = vTable.row(vCurrentTr).data();
           vCourseCode = vDataRow.courseCode
           console.log(vCourseCode)
           var vObjectRequest = gObjectCourses.data
           vObjectRequest.filter(function(index, objectData) {
                   if (index.courseCode === vCourseCode) {
                       vIdCourseDelete = index._id
                       console.log(vIdCourseDelete)
                   }
               })
               // hiển thị modal confirm xóa lên
           $("#delete-confirm-modal").modal("show");
       }


       // Hàm xử lý sự kiện confirm delete course trên modal click
       function onBtnConfirmDeleteCourseClick() {
           //Call API DELETE
           callApiDelete()
               // load lại bảng
           onLoadCoursesToTable(gObjectCourses.data)
               // ẩn modal confirm xóa đi
           $("#delete-confirm-modal").modal("hide");
       }

       // hàm xóa course
       function callApiDelete() {
           $.ajax({
               url: "/courses/" + vIdCourseDelete,
               type: "DELETE",
               dataType: "json",
               success: function(reponseJson) {
                   console.log(reponseJson);
                   alert("DELETE thông tin thành công!");
                   location.reload();
               },
           })
       }