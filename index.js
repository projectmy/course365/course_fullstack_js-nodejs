//Khai báo thư viện express
const express = require('express');

// Import mongooseJS
const mongoose = require('mongoose');

// Import path
const path = require('path');

const courseRouter = require('./app/router/courseRouter');
const { reviewRouter } = require('./app/router/reviewRouter');

//Khai báo app nodeJS
const app = new express();

app.use(express.static(path.join(__dirname + '/app/views')));

//Khai báo middleware json
app.use(express.json());

//Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))

//Khai báo cổng nodeJS
const port = 8000;


mongoose.connect("mongodb://localhost:27017/CRUD_Course365", (err) => {
    if (err) {
        throw err;
    }

    console.log("Connect MongoDB successfully!");
})


//Khai báo API
app.get('/', (request, response) => {

    response.sendFile(path.join(__dirname + '/app/views/courses.html'));
})

app.get('/table', (request, response) => {

    response.sendFile(path.join(__dirname + '/app/views/CRUD.html'));
})

//Sử dụng Router
app.use('/', courseRouter);
app.use('/', reviewRouter);

//chạy trên cổng nodeJS
app.listen(port, () => {
    console.log(`listening on port ${port}`);
})